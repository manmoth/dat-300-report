\chapter{Plan for Implementation}
\section{Web Service}
The web service will be designed using an MVC\footnote{MVC - Model View Controller\\ (\url{http://en.wikipedia.org/wiki/Model-view-controller})}-like setup. This means that whenever a request is made to the server, 
a controller in the MVC framework will handle the request. If the controller needs dynamic data from a data source 
like a database or an external web service, a model will be called to return the data. The result of the request will 
then be output to a view. In a web service, a view isn't quite the correct way of looking at the class exposing the 
data sent from the controller. Output controller is a better description as the output controller only outputs raw 
data to be sent to a client application. None of the data are formatted for humans to view. When the client requests 
data, it should also specify what format of data it will accept in return. The controller that handles the request 
will not take this into concideration, but the output controller will format the output data in accordance to what 
the client reported as acceptable return format.

The CodeIgniter framework offers an easy way of developing the MVC model in PHP by supplying developers with classes 
and methods for handling and routing requests to different controllers, models and views. For a more accurate way 
of looking at the three parts, this report will further down call the parts of the MVC model for \emph{input controller}, 
\emph{model} and \emph{output controller}. This makes more sense when talking about a web service. 

\subsection{Data Source Abstraction}
It is important to abstract the data source from the rest of the web service code in order to simplify the process 
of eventually changing the data source depending on the company the web service is made for. If the code is modular 
and flexible with general methods for contacting the remote data source it should be possible to make this happen. 
It is a challenge to make different data sources with different structures look the same. In order to do this, a system 
for generalization and unification needs to be put down, but even then, some variables will always have to be changed. 
The best solution to this challenge would be to make the data source connection class some kind of a module that can 
be ``installed'' into the web service. This way any programmer can develop the connection part of the data source 
class, and just install that into the web service which then exposes the data from the data source in a general matter.

\subsection{Input Controller}
All requests to the web service will be routed through one file, (\verb|index.php|), and then this files routes the 
request further into the suitable input controller. The input controller will check the request to see if it is valid
and if it is, it will call the model fitting the resource that is being requested. Whenever this call is done, the 
result is parsed by the input controller and sent to the output controller to be formatted and sent back to the requesting 
client. The logic for completing prioritiy PBWS1 will lie in this part of the web service.

\subsection{Model}
A model in MVC terms is the data connection and handling that input controllers can call to get the data they need. 
It can be a database connection to a specific database or table, or it can be a connection to a web service with a 
certain resource. In this project, the models will mostly be connection to the data source that is selected, but in 
some cases it is also need to have data stored in a local database. It is at this level that the modularity regarding 
data sources comes into play. If possible, a class or a framework will be built to handle different data sources while 
still exposing the data in the same way regardless of the data source. The logic for completing priorities PAWS1 and 
PAWS2 will lie in this part of the web service.

\subsection{Output Controller}
Also called View when talking about the original setup of the MVC model. The output controller will look at the 
\verb|accept| field of the request header and output the data accordingly if possible. If not, a default output 
format will be selected and an \verb|HTTP 406 Not Acceptable| response will be sent back. The output controller 
needs to have a general way of interpreting HTTP headers and give HTTP responses accordingly. The REST protocol 
dictates different rules for how to respond to different headers, so a goal is, of course, to comply to all of 
REST's rules and implement the protocol fully.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\textwidth]{../assets/wsrelations.png}
	\caption{Relations between external connections and the web service}
	\label{fig:wsrelations}
\end{figure}

\subsection{Testing}
When testing a web service there are two main areas that needs testing; security and request/response. Is the 
web service in danger of being exploited due to a lack of security? Does it expose data it should not expose? 
Does it respond as expected to all requests? All these question has to be answered before the web service can go 
into production mode. In the scope of this project, only basic testing will be possible due to the time limitations, 
but a thorough security test needs to be done as well as a systematic and well executed request/response test. 
At the time of writing, how to execute these tests is not yet set as there are alot of tools available some better 
than other depending on the needs. A more detailed testing section will be available in the final report for the 
project.

\section{Client}
\subsection{Technical Considerations and Plans}
The graphical user interface can be planned and implemented without having the final web service specifications and this 
will be focus of the first part of the planning period, mainly priority PAC1.  
Programming applications for a mobile platform can be challenging when it comes to GUI, especially when targeting a 
platform with as many different hardware implementations as Android. Therefore the development of the client will be developed 
and testet on a single device for the duration of this project and the display code will not be specifically programmed with 
adaptable layout beyond what is provided by the basic layout and view components.

Much of the client has to be planned after the web service API specification is ready. Since it is so dependent on the web 
service the parts of it that request and use these data need to be planned around the web service API. It also needs to adapt 
to fit the final interface provided by the web service. This will be the next phase of the development project, after finalizing 
the web service API specification.

The client will spend a lot of its energy requesting data and keeping these data updated. The data will need to be parsed and 
manipulated in a way that make them suitable for display to the user. These tasks will be logically separated with clearly 
defined interfaces that allow for clean and modular code.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.2\textwidth]{../assets/clientmodules.png}
	\caption{Separation of modules.}
	\label{fig:clientmodules}
\end{figure}

Beyond this modular separation it is hard to delve too deeply into specifics before the general program structure has been defined. 
It is however worth noting a few problems that will most likely need to be handled along the way.

\begin{itemize}
    \item Actually placing and moving the interactive objects, buses and bus stops, on the map and registering touches for these objects.
    \item Settling on an optimized frequency for updating the data. This involves some tuning during testing later on.
    \item Setting up a connection with the web service and fetching data asynchronously.
\end{itemize}


Much of the map functionality such as centering on position and navigation of the map is a part of the Google Maps API and should not prove 
too difficult, assuming the API works as intended.
This could mean that the problem of placing and moving objects is just a matter of keeping updated longitude and latitude for each object.

The problem of frequency of updating data is related to the inherent delays in internet communication and the cost of data traffic to and 
from mobile units and less battery life from the extra calculations. It is a complex problem in this situation because the data is updated 
real time at a source. The frequency with which this source updates the data is the highest possible frequency the data can be updated, 
ignoring all calculation and transfer costs. Beyond this it is necessary to find a frequency that is practical to the user while keeping 
the data usage and calculation costs down. It could also be a good idea to include a way for the user to manipulate this frequency within 
some set boundaries.

Setting up the connection and fetching the data needs to happen without the GUI freezing up and thus providing frustration to the user. This 
problem is usually a case of creating worker threads to perform network operations while maintaining the GUI and responding to events in a 
different thread. It could however lead to strange glitches and bugs when two threads are accessing the same data and especially visually as 
the data may be in a state that gives an incomplete picture when displayed.
These are issues that must be addressed when testing the applications, but a good start will be to make the display code event based so that 
it responds to events fired by the networking thread and only displays the data when the network thread says it is ready.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\textwidth]{../assets/clientasynch.png}
	\caption{Possible solution to asynchronous update problem.}
	\label{fig:clientasynch}
\end{figure}


\subsection{Testing}
The client application that is produced within the duration of this project shall be considered a prototype. 
This means that it will be subject to bugs and deviations in functionality and specification. However it is 
always a good idea to test software thoroughly and iteratively while developing. There are several levels of 
tests that can be run such as unit level, integration level and system level. The levels that will be tested 
during the development of the client application are unit level and system level. Any unexpected behavior or bug 
shall be documented and corrected or documented and willfully passed over. This last option is only available 
when the issue is both minor and contained within a specific unit (not systemic). The application should also be, 
less formally, evaluated after all changes to see if it follows the design guidelines, Android Develoment best 
practices and the specifications set down in this document. 

\subsubsection{Unit Level}
On the unit level, test classes will be written for each new unit of the application, or earlier tests will be 
modified and updated to test new features. These tests should at least assert expected behavior, whether the 
tested unit follows the specifications set out and whether it fails graciously when presented with unexpected input.
Unit software tests will be designed and implemented based on the general test classes provided by the Android 
Testing Framework. This framework is based on JUnit with special classes for Android specific testing. 

\subsubsection{System Level}
On the system level a more general test (not necessarily a programmed test) will be run to test the entire system 
after the changes made. These tests will be more general and will assert the expected functionality of the entire 
system after a feature (priority) has been implemented.
\subsection{Design}
A large part of the problem when designing an application to be used by people is
the actual design of the user interface and accessing functionality. Fortunately the
Android Developer Guide provides excellent articles and guides for developing a well-
designed application. These practices will be observed closely when designing the client.


\subsection{Client Screens}
When the application is first started a screen is shown with the application name and a logo on top. The 
actionbar, which is a staple throughout the application, will also be shown at the bottom of the screen with the 
most relevant actions immediately available. Other options such as ``Favourites'' and ``Select destination'' are 
available in the action overflow menu. The main attraction in most screens is the map display. This display is 
interactive and shows buses and bus stops as icons in the map that can be touched to show information. When the 
user taps a bus or bus stop on the map relevant information such as route number, time until arrival and bus stop 
name, pops up at the top of the screen replacing the logo and application name. Additionally if the user touches 
the object for a longer while, a popup prompt will show allowing them to add it to favourites, set a notification 
or show a more detailed schedule. Further there will be screens that list destinations and favourite buses and 
bus stops. Below the screens that will be available to the user throughout the application are shown.

The figures show the planned design at the time of writing. However there will most likely be some changes due 
to feedback from user tests done while developing. There is also a likelihood that some things will look 
different due to technical limitations or existing solutions that can not be customized enough to get the desired 
look.

\begin{figure}[p]
	\includegraphics[width=0.6\textwidth]{../assets/client1.png}
	\caption{Explanation of the different buttons on screen. This is also the default screen when no bus or bus stop is selected.}
	\label{fig:client1}
\end{figure}

\begin{figure}[p]
	\includegraphics[width=0.6\textwidth]{../assets/client2.png}
	\caption{Action overflow menu explained.}
	\label{fig:client2}
\end{figure}

\begin{figure}[p]
	\includegraphics[width=0.6\textwidth]{../assets/client3.png}
	\caption{Bus selected. Infobar shows route number, time until arrival and name of next stop. The next stop is also marked by having a different colour in the map.}
	\label{fig:client3}
\end{figure}

\begin{figure}[p]
	\includegraphics[width=0.6\textwidth]{../assets/client4.png}
	\caption{Bus stop selected. Infobar shows a list of the buses that are about to arrive. The specific bus can be selected either by tapping the text when it is showing or the bus in the map. These buses will be marked by having a different colour in the map.}
	\label{fig:client4}
\end{figure}

\begin{figure}[p]
	\includegraphics[width=0.6\textwidth]{../assets/client5.png}
	\caption{This menu is shown when a bus or bus stop is pressed for a longer period.}
	\label{fig:client5}
\end{figure}

\begin{figure}[p]
	\includegraphics[width=0.6\textwidth]{../assets/client6.png}
	\caption{This menu is shown when the favourites button is pressed.}
	\label{fig:client6}
\end{figure}

\begin{figure}[p]
	\includegraphics[width=0.6\textwidth]{../assets/client7.png}
	\caption{This menu is shown when the destination button is pressed. The list of buses is shown, and by entering text into the field the listing is refined.}
	\label{fig:client7}
\end{figure}

\begin{figure}[p]
	\includegraphics[width=0.6\textwidth]{../assets/client8.png}
	\caption{This menu is shown when the Notification button is pressed for a selected bus or bus stop.}
	\label{fig:client8}
\end{figure}