\chapter{Introduction}
\section{Problem Definition}
This project aims to tackle the practical problem of determining whether a given bus is running on time and where 
it is relative to the next stop on its route. This can also be redefined as the problem of relaying real time 
vehicle position data gathered by a mass transit provider to the customer in a practical and useful manner.
The problem separates neatly into two sub-problems; relaying the data to the customer and parsing and displaying 
the data for a customer who is often on the move when most in need of it.

\section{Background}
The problem that is the focus of the project was put forth by Red Rock AS. They are a software company located in 
Grimstad. They provide consultancy services and develop and market software products such as a mobile ticketing 
application for Agder Kollektivtrafikk.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.4\textwidth]{../assets/redrocklogo.png}
	\caption{Red Rock Logo}
	\label{fig:redrocklogo}
\end{figure}

Before setting out to create a presumably novel solution to a problem it is usually a good idea to do a bit of 
research to map the existing (if any) solutions and their strengths and weaknesses in comparison to the 
proposed solution. The comparisons and research done is by no means exhaustive but they are focused on 
the big public transportation companies in the largest cities in Norway and should give a realistic picture 
of how the situation is at the time of writing in the most relevant markets.

\subsection{Motivation}
\subsubsection{Current Situation}
There is widespread support for travel planning and most large companies provide this, there is however not much 
support for real time data to the customer. The ones that do provide data are RuterReise and Kolumbus Sanntid and 
these don't have, or implement poorly, user friendly interfaces, real time bus positions, ticket purchasing and more. 
It is apparent that support for real time updated bus positioning is not widely supported yet and would serve to 
improve many of these applications that already show real time departures and arrivals.

It is interesting to note that the reasons for this functionality not being supported very well may be connected to 
the lack of readily available data or impossibilities/difficulties in implementing it. There exist solutions for the 
business market which allow for complete overviews of a fleet of vehicles, such as Saga System \footref{http://sagasystem.com/service/buss/realtime-3/}, 
but this is not oriented towards the passenger and usually not directly connected to other data such as schedule and 
bus stop. Therefore it is a necessary and interesting goal to see if this is possible to do in a manner that is 
practical, technically possible and accurate.

\subsubsection{Market Opportunities and Business Considerations}
The potential market for such a product can be hard to estimate but it would not be too unrealistic to assume that 
most organizations dealing with passenger transport would be interested in it to some degree. Many of these 
organizations already have, or plan to gather, a lot of available real time data on vehicle positions.

According to the Red Rock representative, Morten R�nning, there is at the time of writing a very large demand at the 
mass transit providers side for a solution to the problem of providing the real time vehicle data to the customer in 
a useful way. Large companies such as Agder Kollektivtrafikk and Vestviken kollektivtrafikk, among others, have expressed 
great interest in an integrated solution with applications that provide services such as schedule information and 
ticket purchasing. 

Red Rock is already deeply invested in the mass transit/passenger transport market with hopes of expanding their 
operations and providing more complete and integrated solutions to their customers. They currently develop and 
maintain a mobile ticket purchasing application for Agder Kollektivtrafikk with close to 5000 active users. Much of 
the motivations behind their interest in the problem of real time data is related to this application. It can benefit 
greatly from an extension which provides updated positions of the buses that are in the area and their customer, AKT, 
has already asked to have this feature implemented.
\newpage
\subsection{Current Solutions}
Most of the public transport providers in well populated regions and cities have ticket purchasing applications 
but only those with either a trip planner or schedule applications have been looked at more closely. Only a 
single publically (and easily accessible) web service was found, although much of the data is probably internally 
available on request or for those developing applications for the specific companies, as is the case with 
Vestviken Kollektivtrafikk who have agreed to share this information with Red Rock and in turn will be available 
for use when developing this project.
For comparison and analysis the following were chosen\footnote{All applications were tested on an Android device}:
\begin{itemize}
  \item Ruter AS both web service and application
  \item Agder Kollektivtransport application
  \item Kolumbus application
  \item Vestviken Kollektivtrafikk application
  \item Skyss (Hordaland) application
\end{itemize}

\subsubsection{Ruter AS}
Ruter or ''trafikanten.no'' provides a very extensive web service \footnote{\href{http://reis.trafikanten.no/ReisRestNational/help}{http://reis.trafikanten.no/ReisRestNational/help}} 
which is based on REST principles and can provide much relevant data such as bus stop positions, schedule and 
real time departures/arrivals. It even has a national web service with data available across the country with the 
same service calls only with a different domain name. This web service is valuable as a comparison when designing 
a similar web service.

The applications \footnote{\href{http://ruter.no/en/Services/mobile/}{http://ruter.no/en/Services/mobile/}} provided by Ruter provide trip planning and ticket 
purchasing. They even provide real time arrival and departure info based on bus stops. However, neither application 
provides a readily available map that can be used to navigate or locate nearby bus stops and they rely too heavily 
on the menu button as a way of accessing key features. Another, minor, drawback is that the button used for 
executing a search is hard to distinguish and should stand out more against the rest of the menu.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\textwidth]{../assets/1ruter.png}
	\caption{Screenshot of the Ruter.no application}
	\label{fig:ruter1}
\end{figure}

\subsubsection{Agder Kollektivtrafikk}
The application\footnote{\href{http://www.agderkollektivtrafikk.no/Article.aspx?m=232\&amid=5902}{http://www.agderkollektivtrafikk.no/Article.aspx?m=232\&amid=5902}} provided by AKT is 
mainly for ticket purchasing but it also provides a map with nearby bus stops relative to the GPS coordinates of 
the device. It is user friendly with a smooth interface that is easy to use but lacks the real time functionality 
that is the focus of the project, namely real time tracking of buses and departures/arrivals.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\textwidth]{../assets/2akt.png}
	\caption{Screenshot of the AKT application}
	\label{fig:akt1}
\end{figure}

\subsubsection{Kolumbus (Rogaland kollektivtrafikk)}
Kolumbus has two separate applications\footnote{\href{http://www.kolumbus.no/index.php?c=161\&kat=app.kolumbus.no}{http://www.kolumbus.no/index.php?c=161\&kat=app.kolumbus.no}} \footnote{\href{https://play.google.com/store/apps/details?id=Kolumbus.Sanntid.Android}{https://play.google.com/store/apps/details?id=Kolumbus.Sanntid.Android}}, 
one is a trip planner with an excellent interface and responsive layout, the second is an app showing real time 
departures/arrivals for selected buss stops. These applications are well structured and the trip planner seems to 
be based on an application format used by the developer\footnote{Bouvet ASA have developed this type of application for at least Kolumbus, Skyss and Vestviken Kollektivtrafikk.} 
for these types of applications. The real time application can help provide some guidelines for how to display the 
data in a user friendly way.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\textwidth]{../assets/3kolumbus.png}
	\caption{Screenshot of the Kolumbus application}
	\label{fig:kol1}
\end{figure}

\subsubsection{Skyss (Hordaland)}
The application from Skyss\footnote{\href{http://www.skyss.no/nn-NO/Rutetider-og-kart/Applikasjon-for-iPhone-og-Android/}{http://www.skyss.no/nn-NO/Rutetider-og-kart/Applikasjon-for-iPhone-og-Android/}} 
is very similar to the one from Kolumbus and as it turns out they are from the same developer and likely based on 
the same format and layout. They also provide a ticket purchasing application but again neither support any real 
time departures/arrivals and bus positioning.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\textwidth]{../assets/4skyss.png}
	\caption{Screenshot of the Skyss application}
	\label{fig:skyss1}
\end{figure}

\subsubsection{Vestviken Kollektivtrafikk}
The travel planning application from VKT\footnote{\href{http://www.vkt.no/App/HvordanvirkerAndroidappen.aspx}{http://www.vkt.no/App/HvordanvirkerAndroidappen.aspx}} is also 
from the same developer as those for Skyss and Kolumbus and provide very similar features and options. There is 
not currently a ticket purchasing application available from VKT.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\textwidth]{../assets/5vkt.png}
	\caption{Screenshot of the VKT application}
	\label{fig:vkt1}
\end{figure}

\section{Project Organization}
\subsection{Environment and Project Set-up}
Project workflow is always a key problem whenever multiple tools, people and technologies are used. If the project workflow is inefficient it can cause frustration and lower morale of the people involved. For these reasons a proper workflow was put down as follows.

\subsubsection{Web Service Workflow}
All files reside on the local system until backed up. The source files are referenced by the Eclipse IDE in a 
local project which is used for writing and formatting the code. The source files are uploaded to the FTP-server 
when testing and also pushed to the Git repository for backup.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.4\textwidth]{../assets/intro_ws_structure.png}
	\caption{Web service workflow}
	\label{fig:wswf}
\end{figure}

\subsubsection{Client workflow}
All project files reside in on the local file system until backed up. 
The source files are referenced from a source controlled folder by the Eclipse IDE and used in a local project. 
The program is compiled from the IDE and run on a connected Android device.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.4\textwidth]{../assets/intro_cl_structure.png}
	\caption{Android Client workflow}
	\label{fig:acwf}
\end{figure}

\subsubsection{Version Control}
The version control that will be used, both for the report and the actual code production, is Git. An alternative to 
Git is Subversion, but lately Git has proven to be the unofficial successor of Subversion due to it having the same
functionality as Subversion with added features and better performance when commiting, merging and patching.\cite{git-speed}
Another advantage of using Git is that more and more professional comapanies use Git as their version control system, 
which means getting used to the protocol is advantageous before entering the industry full-time.

Ideally the workflow of the project will exploit Git's features as much as possible. Having a clean master branch while
having multiple development branches, only merging with the master branch when a feature is done is the accepted practice
and will also be used in this project. Since Git repositories work locally until you explicitly tell Git to upload the
work to a remote server, usually only the master branch will be available remotely, while development branches are all
kept locally on each developer's computer until merged with the master branch.

\subsection{Development and Project Process}
Kanban will be the development process of choice for this project. As there are only two group members and alot of goals
that are hard to define explicitly, Kanban is a natural choice. Since Kanban allows for more ``trial and error'' than
processes like Scrum, it is more fitting for a small team with little experience. To keep track of tasks and work hours, 
Jira will be the tool of choice, even though this is directed more towards larger teams with multiple projects. Jira
is used by Red Rock and other development companies, thus making it a wise choice to get experience with tools that are
actually used professionally.

The group will have weekly meetings with one of, or both, supervisors. In addition to supervisor meetings, a continous
dialog will be kept with Red Rock's representative on the project, Morten R�nning. There will also be sporadic meetings
with Morten whenever he, the supervisors or the group feels the need for this. Since the group will be sitting at Red Rock's
offices two to three days a week, weekly scheduled meetings with Red Rock are not necessary.

Red Rock works with two weeks sprints where every period is initialized by a meeting where issues and tasks are 
defined and given a weight in complexity. When the tasks are set, the team can start assigning tasks to developers 
and calculating time for each task or issue. As most of the work on the project will be done at Red Rock's offices, 
the same workflow will be used for the project, although, with a team consiting of only two people, there will probably 
be room for more flexibility than would have been with larger teams.s

\subsection{Tools}
The entire project will use Eclipse as its main integrated development evironment (IDE). Eclipse is an open-source,
multi-purpose IDE written in Java. This means it is flexible, platform independent and has good help resources available 
using the massive community surrounding it.

For report writing, Eclipse will be used together with a plug-in called ``TeXlipse''.\footnote{\url{http://texlipse.sourceforge.net/}}
All report writing will, in other words, be done using the \LaTeX \ typesetting language.

For web service development, another Eclipse plug-in will be used - Eclipse PDT (PHP Development Tools).\footnote{\url{http://projects.eclipse.org/projects/tools.pdt}}
As the entire web service will be written in PHP using MySQL as the database backend, the Eclipse PDT plug-in is spot on
for this project, even though Eclipse lacks minor features compared to other tools available. One of which is an easy
to use FTP client to directly upload files on save. Competitors like Adobe Dreamweaver and Zend have this capability, 
but these are not open-source. Eclipse also lacks HTML code hints, but this will not be a major issue as the web sevice
will contain little to no HTML code.

The Android client will use the official Android development plug-in released by Google, ADT (Android Development Tools).\footnote{\url{http://developer.android.com/tools/sdk/eclipse-adt.html}}
As much of the syntax in Android development is based on Java and one of the main areas of Eclipse's use is Java, 
Eclipse is a natural choice. All official tutorials and guides on Android development also uses Eclipse as its IDE
of choice. Google has even released an official Eclipse Android bundle for developers to download and use.

\subsection{Documentation}
\subsubsection{Web Service}
Both the internal workings and the external interface will be documented formally for the web service. The 
documentation of the internal workings is particularly important as the code shall be reusable and modifiable later 
on. The external interface must be properly documented as specified below but shall follow the REST principles and 
thus the calls should be logical and understandable without too much reading.

An accepted and usual convention when it comes to documenting RESTful web services is to have a sub-domain 
available for developers to get the documentation needed. Consider an example where the RESTful web service is 
located at \\\verb|api.example.com|. The documentation for its resources will be located, by convention, at 
\verb|developer.example.com|. At this location there will be a listing of available resources, permissions for each 
resource etc. This will be formatted using simple HTML and CSS to make it readable, but an important point is to 
make the information ``to the point'' and minimalistic.

The internal code documentation will be done using phpDocumentor\footref{http://www.phpdoc.org/} which interprets 
specifically formatted comments and produces an indexed list of classes and functions conatined in the written code. 
phpDocumentor's documentation\footref{http://www.phpdoc.org/docs/latest/for-users/phpdoc-reference.html} has a 
complete list of allowed attributes and accepted syntax for comments which is easy to follow and is similar to what 
is found in javaDoc\footref{http://docs.oracle.com/javase/1.4.2/docs/tooldocs/windows/javadoc.html} and similar 
documentation tools.

Since Red Rock hopefully will be using the web service for future projects, proper documentation is key for them to 
be able to continue development with as little time as possible used on reading through code written in this project. 
Documentation of both the web service's resources and code will make this a reality.

\subsubsection{Client}
The client is less in need of formal documentation as it is not meant to be a generally reusable codebase although 
modularity is an important consideration during development. However usage guides and expected user interaction cases 
will be written continually both to provide a user with helpful information and to keep a user focused perspective 
while designing. The code shall be commented using Javadoc syntax but shall be kept simple and inline commenting 
should only be used when the code is particularly dense or based on external examples.
